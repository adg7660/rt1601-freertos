#include <stdio.h>
#include <math.h>

#include "board.h"

#include "pin_mux.h"
#include "clock_config.h"
#include "fsl_gpio.h"
#include "fsl_iomuxc.h"
#include "fsl_flexcan.h"

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"
#include "event_groups.h"

#include "mb_can.h"


#define IS_ODD(x)   ((x & 1) == 1)

#define MAX13050_INTERFACE_DELAY_NS   160
#define BUS_PROP_DELAY_NS             5
#define INTERFACE_PROP_DELAY_NS       MAX13050_INTERFACE_DELAY_NS

#define MAX_NBT 25
#define MIN_NBT 8
#define PRESCALER_MAX   255//(1 << 5)
#define PRESCALER_MIN   0//(1 << 0)

#define PROP_SEG_MAX    8

#define BAUDRATE_MAX        (1000 * 1000) // 1000kbit/s or 1mbit/s
#define BAUDRATE_VARIANCE   (5 * 1000) // 5kbit/s

#define SYNC_SEG    1 // always 1 TQ
#define SJW_MAX     4

struct bit_timings {
    int prescaler;
    int nbt; // Nominal Bit Time (Total number of TQ)
    int prop_seg;
    int phase_seg1;
    int phase_seg2;
    int sjw;
    float osc_tol;
    float sample_point;
    int32_t baudrate;
};

static void print_bt(struct bit_timings *bt) {
    rt_kprintf("---------------------------------------\n");
    rt_kprintf("prescaler: %d\n", bt->prescaler);
    rt_kprintf("NBT: %d\n", bt->nbt);
    rt_kprintf("prop_seg: %d\n", bt->prop_seg);
    rt_kprintf("phase_seg1: %d\n", bt->phase_seg1);
    rt_kprintf("phase_seg2: %d\n", bt->phase_seg2);
    rt_kprintf("sjw: %d\n", bt->sjw);
    rt_kprintf("Oscilator tolrence: %f%%\n", bt->osc_tol);
    rt_kprintf("sample point: %f%%\n", bt->sample_point);
    rt_kprintf("baudrate: %f (kbit/s)\n", bt->baudrate/1000.0);
    rt_kprintf("---------------------------------------\n");
}

int calc_bit_timings(struct bit_timings *bt, int can_clk, int bus_len, int32_t desired_baudrate)
{
	bt->osc_tol = 0;
	int n_found = 0;

	for (int prescaler = PRESCALER_MIN; prescaler <= PRESCALER_MAX; prescaler++)
	{
		if (can_clk % prescaler != 0)
			continue; // must be interger devisible

		for (int nbt = MIN_NBT; nbt <= MAX_NBT; nbt++)
		{
			const int32_t baudrate = can_clk / prescaler / nbt ;/// 1000.0; // in kbit/s
			if (baudrate > BAUDRATE_MAX)
				continue;

			if (abs(baudrate - desired_baudrate) > BAUDRATE_VARIANCE)
				continue;

			//if (baudrate != desired_baudrate)
			//	continue;

			const int bit_time =  (1.0/nbt)*1000; // 1TQ in nanoseconds

			const int bus_delay  = bus_len * BUS_PROP_DELAY_NS;
			const int t_prop_seg = 2 * (bus_delay + INTERFACE_PROP_DELAY_NS);
			const int prop_seg   = ceil(t_prop_seg / (float)bit_time);
			if (prop_seg > PROP_SEG_MAX)
				continue;

			const int phase_segments = nbt - prop_seg - SYNC_SEG;
			int phase_seg1;
			int phase_seg2;
			if (phase_segments < 3) {
				continue;
			} else if (phase_segments == 3) {
				phase_seg1 = 1;
				phase_seg2 = 2;
			} else {
				if (IS_ODD(phase_segments)) {
					phase_seg1 = phase_segments/2; // integer division floors the result
					phase_seg2 = phase_seg1 + 1;
				} else {
					phase_seg2 = phase_seg1 = phase_segments/2;
				}
			}

			const int sjw = fmin(SJW_MAX, phase_seg1);

			const float osc_tol1 = sjw / (float)(10 * nbt);
			const float osc_tol2 = fmin(phase_seg1, phase_seg2) / (float)(2 * (13 * nbt - phase_seg2));
			const float osc_tol  = fmin(osc_tol1, osc_tol2) * 100;

			const float sample_point = (nbt - phase_seg2 ) / (float)nbt * 100;

			if (osc_tol > bt->osc_tol)
			{
				bt->prescaler    = prescaler;
				bt->nbt          = nbt;
				bt->prop_seg     = prop_seg;
				bt->phase_seg1   = phase_seg1;
				bt->phase_seg2   = phase_seg2;
				bt->sjw          = sjw;
				bt->osc_tol      = osc_tol;
				bt->sample_point = sample_point;
				bt->baudrate     = baudrate;
			}

			n_found++;
		}
	}

	return n_found;
}


/*******************************************************************************
 * Variables
 ******************************************************************************/
flexcan_handle_t flexcanHandle;
volatile bool txComplete = false;
volatile bool rxComplete = false;
volatile bool wakenUp    = false;
flexcan_mb_transfer_t txXfer, rxXfer;
#define EXAMPLE_FLEXCAN_IRQn CAN2_IRQn

flexcan_frame_t frame;
uint32_t txIdentifier;
uint32_t rxIdentifier;

EventGroupHandle_t can_AccessEvent;
EventBits_t can_txFlag;
static flexcan_frame_t rx_frame;
static flexcan_fifo_transfer_t flexcan_fifo_transfer;


/*!
 * @brief FlexCAN Call Back function
 */
static void flexcan_callback(CAN_Type *base, flexcan_handle_t *handle, status_t status, uint32_t result, void *userData)
{
	BaseType_t xResult;

	switch (status)
	{
		case kStatus_FLEXCAN_TxBusy:
			rt_kprintf("-->can irq TxBusy\r\n");
			break;

		case kStatus_FLEXCAN_TxIdle:
			{
				//rt_kprintf("-->can irq TxIdle\r\n");
#if 1
				if (TX_MESSAGE_BUFFER_NUM == result)
				{
					txComplete = true;
				}
#else
				portBASE_TYPE taskToWake = pdFALSE;

#ifdef __CA7_REV
				if (SystemGetIRQNestingLevel())
#else
				if (__get_IPSR())
#endif 
				{
					xResult = xEventGroupSetBitsFromISR(can_AccessEvent, can_txFlag, &taskToWake);
					if ((pdPASS == xResult) && (pdTRUE == taskToWake))
					{
						portYIELD_FROM_ISR(taskToWake);
					}
				}
				else
				{
					xEventGroupSetBits(can_AccessEvent, can_txFlag);
				}
#endif
			}
			break;

		case kStatus_FLEXCAN_TxSwitchToRx:
			rt_kprintf("-->can irq TxSwitchToRx\r\n");
			break;

		case kStatus_FLEXCAN_RxBusy:
			rt_kprintf("-->can irq RxBusy\r\n");
			break;

		case kStatus_FLEXCAN_RxIdle:
			//rt_kprintf("-->can irq RxIdle\r\n");
			if (RX_MESSAGE_BUFFER_NUM == result)
			{
				rxComplete = true;
			}
			break;

		case kStatus_FLEXCAN_RxOverflow:
			rt_kprintf("-->can irq RxOverflow\r\n");
			break;

		case kStatus_FLEXCAN_RxFifoBusy:
			rt_kprintf("-->can irq RxFifoBusy\r\n");
			break;

		case kStatus_FLEXCAN_RxFifoIdle:
			//rt_kprintf("-->can irq RxFifoIdle%d, %d\r\n", result, RX_MESSAGE_BUFFER_NUM);
//			if (RX_MESSAGE_BUFFER_NUM == result)
			{
//				rt_kprintf("-->can irq RxIdle2\r\n");
				rxComplete = true;

				flexcan_fifo_transfer.frame = &rx_frame;
				FLEXCAN_TransferReceiveFifoNonBlocking(EXAMPLE_CAN, &flexcanHandle, &flexcan_fifo_transfer);
			}
			break;

		case kStatus_FLEXCAN_RxFifoOverflow:
			rt_kprintf("-->can irq RxFifoOverflow\r\n");
			break;

		case kStatus_FLEXCAN_RxFifoWarning:
			rt_kprintf("-->can irq RxFifoWarning\r\n");
			break;

		case kStatus_FLEXCAN_ErrorStatus:
//			rt_kprintf("-->can irq ErrorStatus\r\n");
			rt_kprintf(".");
			break;

		case kStatus_FLEXCAN_WakeUp:
			rt_kprintf("-->can irq WakeUp\r\n");
			break;

		case kStatus_FLEXCAN_UnHandled:
			rt_kprintf("-->can irq UnHandled\r\n");
			break;

		case kStatus_FLEXCAN_RxRemote:
			rt_kprintf("-->can irq RxRemote\r\n");
			break;

		default:
			rt_kprintf("-->can irq unknow state:%d\r\n", status);
			break;
	}
}

typedef struct {
	
} mb_can_t;

int search_ps_and_baudrate()
{
	
}

uint32_t get_sp(uint8_t propseg, uint8_t pseg1, uint8_t pseg2)
{
	uint8_t tseg1 = 0;
	uint8_t tseg2 = 0;
	uint8_t tq = 0;
	float sp = 0.0f;

	tseg1 = propseg + pseg1 + 2;
	tseg2 = pseg2 + 1;
	tq = 1 + tseg1 + tseg2;

	sp = (float)(1 + tseg1)/(float)tq;

	return (uint32_t)(sp*10000);
}

void can_list_timing_info(uint32_t br, uint32_t can_clk)
{
	uint8_t propseg = 0;	//0~7
	uint8_t pseg1 = 0;		//0~7
	uint8_t pseg2 = 0;		//1~7
	uint8_t rjw = 0;		//0~3
	uint8_t presdiv = 0;	//0~255

	uint8_t tseg1 = 0;
	uint8_t tseg2 = 0;
	uint8_t tq = 0;
	float sp = 0.0f;
	float ftmp = 0.0f;

/*
	TimeSegment1	TimeSegment2	Re-synchronizationJumpWidth
	5~10			2				1~2
	4~11			3				1~3
	5~12			4				1~4
	6~13			5				1~4
	7~14			6				1~4
	8~15			7				1~4
	9~16			8				1~4
*/

/*
	采样点CiA推荐的值：
	75% when 波特率 > 800K
	80% when 波特率 > 500K
	87.5% when 波特率 <= 500K
*/

	for(propseg=0; propseg<=7; propseg++)
	{
		for(pseg1=0; pseg1<=7; pseg1++)
		{
			for(pseg2=1; pseg2<=7; pseg2++)
			{
				tseg1 = propseg + pseg1 + 2;
				tseg2 = pseg2 + 1;
				tq = 1 + tseg1 + tseg2;

				if(tseg1>=4 && tseg1<=16)
				{
					if(tseg2>=2 && tseg2<=8)
					{
						if(tq>=8 && tq<=25)
						{
							sp = (float)(1 + tseg1)/(float)tq;
							//if(sp >= 0.80f && sp <= 0.85f)
							if(sp >= 0.70f && sp <= 0.85f)
							{
								rt_kprintf("<calc>propseg:%d, pseg1:%d, pseg2:%d, rjw:%d, sp:%d\r\n", \
									propseg, pseg1, pseg2, 0, (uint32_t)(sp*10000));
							}
						}
					}
				}
			}
		}
	}
}

int mb_can_hw_init(void)
{
	flexcan_config_t flexcanConfig;
	flexcan_rx_mb_config_t mbConfig;
	flexcan_timing_config_t *pt;
	uint8_t node_type;

	/*Clock setting for FLEXCAN*/
	CLOCK_SetMux(kCLOCK_CanMux, FLEXCAN_CLOCK_SOURCE_SELECT);
	CLOCK_SetDiv(kCLOCK_CanDiv, FLEXCAN_CLOCK_SOURCE_DIVIDER);

	txIdentifier = 0x321;
	rxIdentifier = 0x321;

	/* Get FlexCAN module default Configuration. */
	/*
	* flexcanConfig.clkSrc                 = kFLEXCAN_ClkSrc0;
	* flexcanConfig.baudRate               = 1000000U;
	* flexcanConfig.baudRateFD             = 2000000U;
	* flexcanConfig.maxMbNum               = 16;
	* flexcanConfig.enableLoopBack         = false;
	* flexcanConfig.enableSelfWakeup       = false;
	* flexcanConfig.enableIndividMask      = false;
	* flexcanConfig.disableSelfReception   = false;
	* flexcanConfig.enableListenOnlyMode   = false;
	* flexcanConfig.enableDoze             = false;
	*/
	FLEXCAN_GetDefaultConfig(&flexcanConfig);

	flexcanConfig.clkSrc = kFLEXCAN_ClkSrc0;
	flexcanConfig.baudRate = USED_BAUDRATE;
	flexcanConfig.disableSelfReception = false;
	flexcanConfig.enableLoopBack = true;

	/* If special quantum setting is needed, set the timing parameters. */
#if (defined(SET_CAN_QUANTUM) && SET_CAN_QUANTUM)
	flexcanConfig.timingConfig.phaseSeg1 = PSEG1;
	flexcanConfig.timingConfig.phaseSeg2 = PSEG2;
	flexcanConfig.timingConfig.propSeg   = PROPSEG;
#if (defined(FSL_FEATURE_FLEXCAN_HAS_FLEXIBLE_DATA_RATE) && FSL_FEATURE_FLEXCAN_HAS_FLEXIBLE_DATA_RATE)
	flexcanConfig.timingConfig.fphaseSeg1 = FPSEG1;
	flexcanConfig.timingConfig.fphaseSeg2 = FPSEG2;
	flexcanConfig.timingConfig.fpropSeg   = FPROPSEG;
#endif
#endif

	pt = &(flexcanConfig.timingConfig);
#if 1//(defined(USE_IMPROVED_TIMING_CONFIG) && USE_IMPROVED_TIMING_CONFIG)
	flexcan_timing_config_t timing_config;
	memset(&timing_config, 0, sizeof(flexcan_timing_config_t));
	if (FLEXCAN_CalculateImprovedTimingValues(flexcanConfig.baudRate, EXAMPLE_CAN_CLK_FREQ, &timing_config))
	{
		/* Update the improved timing configuration*/
		memcpy(&(flexcanConfig.timingConfig), &timing_config, sizeof(flexcan_timing_config_t));
	}
	else
	{
		rt_kprintf("No found Improved Timing Configuration. Just used default configuration\r\n\r\n");
	}
#else
	const int bus_len = 5; // in meters

	struct bit_timings bt;
	const int n = calc_bit_timings(&bt, EXAMPLE_CAN_CLK_FREQ, bus_len, USED_BAUDRATE);
	print_bt(&bt);
	rt_kprintf("Found %d possible bit timings\n", n);

	pt->propSeg = bt.prop_seg-1;
	pt->phaseSeg1 = bt.phase_seg1-1;
	pt->phaseSeg2 = bt.phase_seg2-1;
	pt->rJumpwidth = bt.sjw-1;
	pt->preDivider = bt.prescaler-1;
#endif

	rt_kprintf("can_clk:%d, baudrate:%d\r\n", EXAMPLE_CAN_CLK_FREQ, USED_BAUDRATE);
	rt_kprintf("<real>propseg:%d, pseg1:%d, pseg2:%d, rjw:%d, prediv:%d, sp:%d\r\n", \
			pt->propSeg, pt->phaseSeg1, pt->phaseSeg2, pt->rJumpwidth, pt->preDivider, get_sp(pt->propSeg, pt->phaseSeg1, pt->phaseSeg2));
//	rt_kprintf("<real>propseg:%d, pseg1:%d, pseg2:%d, rjw:%d, sp:%d\r\n", \
//			8, 5, 6, pt->rJumpwidth, get_sp(8, 5, 6));
//	can_list_timing_info(100000, EXAMPLE_CAN_CLK_FREQ);
	FLEXCAN_Init(EXAMPLE_CAN, &flexcanConfig, EXAMPLE_CAN_CLK_FREQ);

	/* Create FlexCAN handle structure and set call back function. */
//	FLEXCAN_TransferCreateHandle(EXAMPLE_CAN, &flexcanHandle, flexcan_callback, NULL);

	/* Setup Tx Message Buffer. */
	FLEXCAN_SetTxMbConfig(EXAMPLE_CAN, TX_MESSAGE_BUFFER_NUM, true);

#if 1
	/* Set Rx Masking mechanism. */
	FLEXCAN_SetRxMbGlobalMask(EXAMPLE_CAN, FLEXCAN_RX_MB_STD_MASK(0, 0, 0));

	/* Setup Rx Message Buffer. */
	mbConfig.format = kFLEXCAN_FrameFormatStandard;
	mbConfig.type	= kFLEXCAN_FrameTypeData;
	mbConfig.id 	= FLEXCAN_ID_STD(rxIdentifier);
	FLEXCAN_SetRxMbConfig(EXAMPLE_CAN, RX_MESSAGE_BUFFER_NUM, &mbConfig, true);
#else
	flexcan_rx_fifo_config_t rxFifoConfig;
	uint32_t rxFifoFilter[] = {FLEXCAN_RX_FIFO_STD_FILTER_TYPE_A(0x321, 0, 0)};

	/* Sets up the receive FIFO. */
	rxFifoConfig.idFilterTable = rxFifoFilter;
	rxFifoConfig.idFilterType  = kFLEXCAN_RxFifoFilterTypeA;
	rxFifoConfig.idFilterNum   = sizeof(rxFifoFilter) / sizeof(rxFifoFilter[0]);
	rxFifoConfig.priority	   = kFLEXCAN_RxFifoPrioHigh;
	FLEXCAN_SetRxFifoConfig(EXAMPLE_CAN, &rxFifoConfig, true);

	/* Setup Rx Message Buffer. */
	mbConfig.format = kFLEXCAN_FrameFormatStandard;
	mbConfig.type	= kFLEXCAN_FrameTypeData;
	mbConfig.id 	= FLEXCAN_ID_STD(rxIdentifier);
	FLEXCAN_SetRxMbConfig(EXAMPLE_CAN, RX_MESSAGE_BUFFER_NUM, &mbConfig, true);

	FLEXCAN_SetRxFifoGlobalMask(EXAMPLE_CAN, FLEXCAN_RX_FIFO_STD_MASK_TYPE_A(0, 0, 0));
	//FLEXCAN_SetRxMbGlobalMask(EXAMPLE_CAN, FLEXCAN_RX_MB_STD_MASK(0, 0, 0));

	flexcan_fifo_transfer.frame = &rx_frame;
	FLEXCAN_TransferReceiveFifoNonBlocking(EXAMPLE_CAN, &flexcanHandle, &flexcan_fifo_transfer);
#endif

	/* Enable Rx Message Buffer interrupt. */
	FLEXCAN_EnableMbInterrupts(EXAMPLE_CAN, 1 << RX_MESSAGE_BUFFER_NUM);
	EnableIRQ(EXAMPLE_FLEXCAN_IRQn);

	return 0;
}

uint32_t offset_tx_count = 0;
uint32_t offset_rx_count = 0;
static uint32_t real_tx_count = 0;
static uint32_t real_rx_count = 0;

flexcan_frame_t txFrame, rxFrame;
#if 1
void CAN2_IRQHandler(void)
{
	/* If new data arrived. */
	if (FLEXCAN_GetMbStatusFlags(EXAMPLE_CAN, 1 << RX_MESSAGE_BUFFER_NUM))
	{
		FLEXCAN_ClearMbStatusFlags(EXAMPLE_CAN, 1 << RX_MESSAGE_BUFFER_NUM);
		FLEXCAN_ReadRxMb(EXAMPLE_CAN, RX_MESSAGE_BUFFER_NUM, &rxFrame);
		real_rx_count++;

		rxComplete = true;
	}
	SDK_ISR_EXIT_BARRIER;
}
#endif

void can_process_thread(void *arg)
{
	static uint8_t count = 0;
	static uint32_t last_tick = 0;
	static uint32_t last_tx_count = 0;
	static uint32_t last_rx_count = 0;

	uint32_t cur_tick = 0;

	last_tick = xTaskGetTickCount();

	while(1)
	{
//		rt_kprintf("-->can_process_thread:%d\r\n", ++count);
		//vTaskDelay(1000);

		cur_tick  = xTaskGetTickCount();
		if((cur_tick - last_tick) > 1000)
		{
			offset_tx_count = real_tx_count - last_tx_count;
			last_tx_count = real_tx_count;

			offset_rx_count = real_rx_count - last_rx_count;
			last_rx_count = real_rx_count;
			last_tick = cur_tick;
		}

		frame.id     = FLEXCAN_ID_STD(txIdentifier);
		frame.format = (uint8_t)kFLEXCAN_FrameFormatStandard;
		frame.type   = (uint8_t)kFLEXCAN_FrameTypeData;
		frame.length = (uint8_t)DLC;

		frame.dataByte0 = count;

		txXfer.mbIdx = (uint8_t)TX_MESSAGE_BUFFER_NUM;
		txXfer.frame = &frame;
#if 0
		(void)FLEXCAN_TransferSendNonBlocking(EXAMPLE_CAN, &flexcanHandle, &txXfer);

		while (!txComplete)
		{
		};
//		xEventGroupWaitBits(can_AccessEvent, can_txFlag, pdTRUE, (BaseType_t)false, portMAX_DELAY);

#else
		FLEXCAN_TransferSendBlocking(EXAMPLE_CAN, TX_MESSAGE_BUFFER_NUM, &frame);

		//rt_kprintf("-->can irq Txdone\r\n");

		/* Waiting for Message receive finish. */
//		while (!rxComplete)
//		{
//		}
//		rxComplete = false;
#endif
		txComplete = false;
		real_tx_count++;

#if 0
		/* Start receive data through Rx Message Buffer. */
		rxXfer.mbIdx = (uint8_t)RX_MESSAGE_BUFFER_NUM;
		rxXfer.frame = &frame;
		(void)FLEXCAN_TransferReceiveNonBlocking(EXAMPLE_CAN, &flexcanHandle, &rxXfer);

		/* Wait until Rx MB full. */
		while (!rxComplete)
		{
		};
		rxComplete = false;
		real_rx_count++;

//		rt_kprintf("Rx MB ID: 0x%3x, Rx MB data: 0x%x, Time stamp: %d\r\n", frame.id >> CAN_ID_STD_SHIFT,
//						frame.dataByte0, frame.timestamp);
//		rt_kprintf("Press any key to trigger the next transmission!\r\n\r\n");
		frame.dataByte0++;
		frame.dataByte1 = 0x55;
#endif
	}
}


TaskHandle_t can_th;

void mb_can_init(void)
{
	can_AccessEvent = xEventGroupCreate();
	can_txFlag = 0x1;

	mb_can_hw_init();

	xTaskCreate(can_process_thread,
				"can",
				256,
				NULL,
				8,//17,
				&can_th);
}

