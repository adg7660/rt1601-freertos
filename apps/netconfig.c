#include <stdio.h>
#include "lwip/opt.h"

#include "lwip/dhcp.h"
#include "lwip/ip_addr.h"
#include "lwip/netifapi.h"
#include "lwip/prot/dhcp.h"
#include "lwip/tcpip.h"
#include "lwip/sys.h"
#include "enet_ethernetif.h"

#include "board.h"

#include "pin_mux.h"
#include "clock_config.h"
#include "fsl_gpio.h"
#include "fsl_iomuxc.h"
#include "fsl_phy.h"
#include "lwip/apps/mdns.h"


#include "netconfig.h"

/* MAC address configuration. */
#define configMAC_ADDR0                    \
	{                                      \
	    0x02, 0x12, 0x13, 0x10, 0x15, 0x11 \
	}
#define configMAC_ADDR1                    \
	{                                      \
	    0x02, 0x12, 0x13, 0x10, 0x15, 0x22 \
	}

#define MDNS_HOSTNAME "lwip-http"

void delay(void)
{
	volatile uint32_t i = 0;
	for (i = 0; i < 1000000; ++i)
	{
		__asm("NOP"); /* delay */
	}
}

void tcpip_init_done_func(void *arg)
{
	rt_kprintf("--> %s\r\n", __FUNCTION__);
}

void phy_read(ENET_Type *base, uint32_t phy_address, uint32_t phyReg)
{
	uint32_t data;
	status_t status;

	status = PHY_Read(base, phy_address, phyReg, &data);
	if (kStatus_Success == status)
	{
		rt_kprintf("PHY_Read: %02X --> %08X\r\n", phyReg, data);
	}
	else
	{
		rt_kprintf("PHY_Read: %02X --> faild\r\n", phyReg);
	}
}

void phy_write(ENET_Type *base, uint32_t phy_address, uint32_t phyReg, uint32_t data)
{
	status_t status;

	status = PHY_Write(base, phy_address, phyReg, data);
	if (kStatus_Success == status)
	{
		rt_kprintf("PHY_Write: %02X --> %08X\r\n", phyReg, data);
	}
	else
	{
		rt_kprintf("PHY_Write: %02X --> faild\r\n", phyReg);
	}
}

void phy_dump(ENET_Type *base, uint32_t phy_address)
{
	uint32_t data;
	status_t status;

	int i;
	for (i = 0; i < 32; i++)
	{
		status = PHY_Read(base, phy_address, i, &data);
		if (kStatus_Success != status)
		{
			rt_kprintf("phy_dump: %02X --> faild", i);
			break;
		}

		if (i % 8 == 7)
		{
			rt_kprintf("%02X --> %08X ", i, data);
		}
		else
		{
			rt_kprintf("%02X --> %08X\r\n", i, data);
		}
	}
}

void enet_reg_dump(ENET_Type *base)
{
	ENET_Type *enet_base = base;

#define DUMP_REG(__REG)  \
	rt_kprintf("%s(%08X): %08X\r\n", #__REG, (uint32_t)&enet_base->__REG, enet_base->__REG)

	DUMP_REG(EIR);
	DUMP_REG(EIMR);
	DUMP_REG(RDAR);
	DUMP_REG(TDAR);
	DUMP_REG(ECR);
	DUMP_REG(MMFR);
	DUMP_REG(MSCR);
	DUMP_REG(MIBC);
	DUMP_REG(RCR);
	DUMP_REG(TCR);
	DUMP_REG(PALR);
	DUMP_REG(PAUR);
	DUMP_REG(OPD);
	DUMP_REG(TXIC);
	DUMP_REG(RXIC);
	DUMP_REG(IAUR);
	DUMP_REG(IALR);
	DUMP_REG(GAUR);
	DUMP_REG(GALR);
	DUMP_REG(TFWR);
	DUMP_REG(RDSR);
	DUMP_REG(TDSR);
	DUMP_REG(MRBR);
	DUMP_REG(RSFL);
	DUMP_REG(RSEM);
	DUMP_REG(RAEM);
	DUMP_REG(RAFL);
	DUMP_REG(TSEM);
	DUMP_REG(TAEM);
	DUMP_REG(TAFL);
	DUMP_REG(TIPG);
	DUMP_REG(FTRL);
	DUMP_REG(TACC);
	DUMP_REG(RACC);
	DUMP_REG(RMON_T_DROP);
	DUMP_REG(RMON_T_PACKETS);
	DUMP_REG(RMON_T_BC_PKT);
	DUMP_REG(RMON_T_MC_PKT);
	DUMP_REG(RMON_T_CRC_ALIGN);
	DUMP_REG(RMON_T_UNDERSIZE);
	DUMP_REG(RMON_T_OVERSIZE);
	DUMP_REG(RMON_T_FRAG);
	DUMP_REG(RMON_T_JAB);
	DUMP_REG(RMON_T_COL);
	DUMP_REG(RMON_T_P64);
	DUMP_REG(RMON_T_P65TO127);
	DUMP_REG(RMON_T_P128TO255);
	DUMP_REG(RMON_T_P256TO511);
	DUMP_REG(RMON_T_P512TO1023);
	DUMP_REG(RMON_T_P1024TO2047);
	DUMP_REG(RMON_T_P_GTE2048);
	DUMP_REG(RMON_T_OCTETS);
	DUMP_REG(IEEE_T_DROP);
	DUMP_REG(IEEE_T_FRAME_OK);
	DUMP_REG(IEEE_T_1COL);
	DUMP_REG(IEEE_T_MCOL);
	DUMP_REG(IEEE_T_DEF);
	DUMP_REG(IEEE_T_LCOL);
	DUMP_REG(IEEE_T_EXCOL);
	DUMP_REG(IEEE_T_MACERR);
	DUMP_REG(IEEE_T_CSERR);
	DUMP_REG(IEEE_T_SQE);
	DUMP_REG(IEEE_T_FDXFC);
	DUMP_REG(IEEE_T_OCTETS_OK);
	DUMP_REG(RMON_R_PACKETS);
	DUMP_REG(RMON_R_BC_PKT);
	DUMP_REG(RMON_R_MC_PKT);
	DUMP_REG(RMON_R_CRC_ALIGN);
	DUMP_REG(RMON_R_UNDERSIZE);
	DUMP_REG(RMON_R_OVERSIZE);
	DUMP_REG(RMON_R_FRAG);
	DUMP_REG(RMON_R_JAB);
	DUMP_REG(RMON_R_RESVD_0);
	DUMP_REG(RMON_R_P64);
	DUMP_REG(RMON_R_P65TO127);
	DUMP_REG(RMON_R_P128TO255);
	DUMP_REG(RMON_R_P256TO511);
	DUMP_REG(RMON_R_P512TO1023);
	DUMP_REG(RMON_R_P1024TO2047);
	DUMP_REG(RMON_R_P_GTE2048);
	DUMP_REG(RMON_R_OCTETS);
	DUMP_REG(IEEE_R_DROP);
	DUMP_REG(IEEE_R_FRAME_OK);
	DUMP_REG(IEEE_R_CRC);
	DUMP_REG(IEEE_R_ALIGN);
	DUMP_REG(IEEE_R_MACERR);
	DUMP_REG(IEEE_R_FDXFC);
	DUMP_REG(IEEE_R_OCTETS_OK);
	DUMP_REG(ATCR);
	DUMP_REG(ATVR);
	DUMP_REG(ATOFF);
	DUMP_REG(ATPER);
	DUMP_REG(ATCOR);
	DUMP_REG(ATINC);
	DUMP_REG(ATSTMP);
	DUMP_REG(TGSR);
}


#define ETH0_RST_PORT		GPIO1
#define ETH0_RST_PIN		2
#define ETH0_RST_GPIO		ETH0_RST_PORT, ETH0_RST_PIN

#define ETH1_RST_PORT		GPIO1
#define ETH1_RST_PIN		3
#define ETH1_RST_GPIO		ETH1_RST_PORT, ETH1_RST_PIN

/*!
 * @brief Callback function to generate TXT mDNS record for HTTP service.
 */
static void http_srv_txt(struct mdns_service *service, void *txt_userdata)
{
    mdns_resp_add_service_txtitem(service, "path=/", 6);
}

void netconfig(void)
{
	static struct netif fsl_netif0;
	static struct netif fsl_netif1;

	ip4_addr_t fsl_netif0_ipaddr, fsl_netif0_netmask, fsl_netif0_gw;
	ip4_addr_t fsl_netif1_ipaddr, fsl_netif1_netmask, fsl_netif1_gw;

	ethernetif_config_t fsl_enet_config0 = {
		.phyAddress = BOARD_ENET0_PHY_ADDRESS,
		.clockName  = kCLOCK_CoreSysClk,
		.macAddress = configMAC_ADDR0,
	};

	ethernetif_config_t fsl_enet_config1 = {
		.phyAddress = BOARD_ENET1_PHY_ADDRESS,
		.clockName  = kCLOCK_CoreSysClk,
		.macAddress = configMAC_ADDR1,
	};

	gpio_pin_config_t gpio_config = {kGPIO_DigitalOutput, 0, kGPIO_NoIntmode};

	tcpip_init(NULL, NULL);






	IOMUXC_EnableMode(IOMUXC_GPR, kIOMUXC_GPR_ENET1TxClkOutputDir, true);

	GPIO_PinInit(ETH0_RST_GPIO, &gpio_config);
	GPIO_PinInit(GPIO1, 10, &gpio_config);
	/* pull up the ENET_INT before RESET. */
	GPIO_WritePinOutput(GPIO1, 10, 1);
	GPIO_WritePinOutput(ETH0_RST_GPIO, 0);
	delay();
	GPIO_WritePinOutput(ETH0_RST_GPIO, 1);

	IP4_ADDR(&fsl_netif0_ipaddr, 192U, 168U, 1U, 236U);
	IP4_ADDR(&fsl_netif0_netmask, 255U, 255U, 255U, 0U);
	IP4_ADDR(&fsl_netif0_gw, 192U, 168U, 1U, 1U);

	netifapi_netif_add(&fsl_netif0, &fsl_netif0_ipaddr, &fsl_netif0_netmask, &fsl_netif0_gw, &fsl_enet_config0,
						ethernetif0_init, tcpip_input);
	netifapi_netif_set_default(&fsl_netif0);
	netifapi_netif_set_up(&fsl_netif0);
	//ENET_ActiveRead(ENET);


	LOCK_TCPIP_CORE();
	mdns_resp_init();
	mdns_resp_add_netif(&fsl_netif0, MDNS_HOSTNAME);
	mdns_resp_add_service(&fsl_netif0, MDNS_HOSTNAME, "_http", DNSSD_PROTO_TCP, 80, http_srv_txt, NULL);
	UNLOCK_TCPIP_CORE();

	rt_kprintf("IP     :%s\r\n", ipaddr_ntoa(&fsl_netif0.ip_addr));
	rt_kprintf("Netmask:%s\r\n", ipaddr_ntoa(&fsl_netif0.netmask));
	rt_kprintf("Gatway :%s\r\n", ipaddr_ntoa(&fsl_netif0.gw));
	rt_kprintf("mDNS hostname :%s\r\n", MDNS_HOSTNAME);


//	phy_dump(ENET2, BOARD_ENET0_PHY_ADDRESS);
//	enet_reg_dump(ENET);
//	rt_kprintf("\r\n");




#if 0

	rt_kprintf("\r\n");

	IOMUXC_EnableMode(IOMUXC_GPR, kIOMUXC_GPR_ENET2TxClkOutputDir, true);

	GPIO_PinInit(ETH1_RST_GPIO, &gpio_config);
	GPIO_PinInit(GPIO1, 10, &gpio_config);
	/* pull up the ENET_INT before RESET. */
	GPIO_WritePinOutput(GPIO1, 10, 1);
	GPIO_WritePinOutput(ETH1_RST_GPIO, 0);
	delay();
	GPIO_WritePinOutput(ETH1_RST_GPIO, 1);

	IP4_ADDR(&fsl_netif1_ipaddr, 192U, 168U, 1U, 237U);
	IP4_ADDR(&fsl_netif1_netmask, 255U, 255U, 255U, 0U);
	IP4_ADDR(&fsl_netif1_gw, 192U, 168U, 1U, 1U);

	netifapi_netif_add(&fsl_netif1, &fsl_netif1_ipaddr, &fsl_netif1_netmask, &fsl_netif1_gw, &fsl_enet_config1,
						ethernetif1_init, tcpip_input);
	netifapi_netif_set_default(&fsl_netif1);
	netifapi_netif_set_up(&fsl_netif1);
	//ENET_ActiveRead(ENET2);

	rt_kprintf("IP     :%s\r\n", ipaddr_ntoa(&fsl_netif1.ip_addr));
	rt_kprintf("Netmask:%s\r\n", ipaddr_ntoa(&fsl_netif1.netmask));
	rt_kprintf("Gatway :%s\r\n", ipaddr_ntoa(&fsl_netif1.gw));

//	phy_dump(ENET2, BOARD_ENET1_PHY_ADDRESS);
//	enet_reg_dump(ENET2);
//	rt_kprintf("\r\n");
#endif
}

