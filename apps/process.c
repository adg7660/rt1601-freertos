#include <stdio.h>
#include "lwip/opt.h"

#include "board.h"

#include "lwip/dhcp.h"
#include "lwip/ip_addr.h"
#include "lwip/netifapi.h"
#include "lwip/prot/dhcp.h"
#include "lwip/tcpip.h"
#include "lwip/sys.h"
#include "enet_ethernetif.h"

#include "lwip/sockets.h"

#include "process.h"
#include "libcsv.h"

typedef enum {
	NET_MODE_TCP_CLIENT = 0,
	NET_MODE_TCP_SERVER,

	NET_MODE_UDP_CLIENT,
	NET_MODE_UDP_SERVER,

	NET_MODE_UDP_MULTICAST,
	NET_MODE_UDP_BROADCAST,

	NET_MODE_RAW,
} network_mode_t;

#define MAX_CONNET_NUM		8

int sock_table[MAX_CONNET_NUM];
network_mode_t network_mode = NET_MODE_TCP_SERVER;

#define SERVER_HOST		"192.168.1.230"
#define SERVER_PORT		12345

void network_tcp_client_connect(void)
{
	uint8_t sock_idx = 0;
	struct sockaddr_in addr;
	size_t len;
	int opt;
	int result = -1;

	for(sock_idx=0; sock_idx<1/*MAX_CONNET_NUM*/; sock_idx++)
	{
		if(sock_table[sock_idx] != -1)
		{
			continue;
		}

		sock_table[sock_idx] = socket(AF_INET, SOCK_STREAM, IPPROTO_IP);

		memset(&addr, 0, sizeof(addr));
		addr.sin_len = sizeof(addr);
		addr.sin_family = AF_INET;
		addr.sin_port = htons(SERVER_PORT);
		addr.sin_addr.s_addr = inet_addr(SERVER_HOST);
		result = connect(sock_table[sock_idx], (struct sockaddr *)&addr, sizeof(addr));
		if(result != 0)
		{
			rt_kprintf("socket idx:%d sock:%d connect error:%d\r\n", sock_idx, sock_table[sock_idx], result);

			close(sock_table[sock_idx]);
			sock_table[sock_idx] = -1;

			vTaskDelay(1000);
			continue;
		}

		opt = 100; //set recv timeout
		result = setsockopt(sock_table[sock_idx], SOL_SOCKET, SO_RCVTIMEO, &opt, sizeof(int));
//		if(result != 0)
//		{
//			rt_kprintf("socket idx:%d sock:%d setsockopt SO_RCVTIMEO error:%d\r\n", sock_idx, sock_table[sock_idx], result);
//
//			close(sock_table[sock_idx]);
//			sock_table[sock_idx] = -1;
//
//			vTaskDelay(1000);
//			continue;
//		}

		rt_kprintf("socket idx:%d socket:%d connet ok.\r\n", sock_idx, sock_table[sock_idx]);
	}
}

void network_tcp_server_connect(void)
{
	int server_sock = -1;
	struct sockaddr_in server_addr;
	struct sockaddr_in client_addr;
	int client_fd = -1;
	int sock_idx = 0;
	int opt = 0;
	int result = 0;

	socklen_t cliaddr_len = sizeof(client_addr);

	server_sock = socket(AF_INET, SOCK_STREAM, IPPROTO_IP);

	opt = 1;
	result = lwip_setsockopt(server_sock, IPPROTO_TCP, TCP_NODELAY, (const void *)&opt, sizeof(opt));
	if (result)
	{
		rt_kprintf("server sock:%d setsockopt TCP_NODELAY error:%d\r\n", server_sock, result);

		close(server_sock);

		vTaskDelay(1000);
		return;
	}

	opt = 1;
	result = setsockopt(server_sock, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));
	if(result)
	{
		rt_kprintf("server sock:%d setsockopt SO_REUSEADDR error:%d\r\n", server_sock, result);

		close(server_sock);

		vTaskDelay(1000);
		return;
	}

	memset(&server_addr, 0, sizeof(server_addr));

	server_addr.sin_family = AF_INET;
	server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	server_addr.sin_port = htons(31001);
	if(bind(server_sock, (struct sockaddr *)&server_addr, sizeof(server_addr)) == -1)
	{
		rt_kprintf("tcp server bind fail\r\n");

		vTaskDelay(1000);
		close(server_sock);
		return;
	}

	if(listen(server_sock, MAX_CONNET_NUM+1) == -1)
	{
		rt_kprintf("tcp server listen fail\r\n");

		vTaskDelay(1000);
		close(server_sock);
		return;
	}

	while(1)
	{
		if((client_fd = accept(server_sock, (struct sockaddr *)&client_addr, &cliaddr_len)) == -1)
		{
			rt_kprintf("tcp server accept fail\r\n");

			vTaskDelay(1000);
			continue;
		}

		for(sock_idx=0; sock_idx<MAX_CONNET_NUM; sock_idx++)
		{
			if(sock_table[sock_idx] == -1)
			{
				break;
			}
		}

		if(sock_idx == MAX_CONNET_NUM)
		{
			rt_kprintf("tcp server connect num out range\r\n");
			vTaskDelay(1000);
			close(client_fd);
		}
		else
		{
			rt_kprintf("tcp server connect success, sock:%d\r\n", client_fd);
			sock_table[sock_idx] = client_fd;
		}
	}
}

void network_udp_client_connect(void)
{
	
}

void network_udp_server_connect(void)
{
	
}


void network_udp_multicast_connect(void)
{
	
}

void network_udp_broadcast_connect(void)
{
	
}



static void network_keep_connect(void *arg)
{
	char *mode_name[] = {"tcp_client", "tcp_server", "udp_client", "udp_server", "udp_multicast", "udp_broadcast", "raw"};

	rt_kprintf("network_keep_connect mode:%s\r\n", mode_name[network_mode]);

	while(1)
	{
		switch(network_mode)
		{
			case NET_MODE_TCP_CLIENT:
				network_tcp_client_connect();
				vTaskDelay(1000);
				break;

			case NET_MODE_TCP_SERVER:
				network_tcp_server_connect();
				vTaskDelay(1000);
				break;

			case NET_MODE_UDP_CLIENT:
				network_udp_client_connect();
				vTaskDelay(1000);
				break;

			case NET_MODE_UDP_SERVER:
				network_udp_server_connect();
				vTaskDelay(1000);
				break;

			case NET_MODE_UDP_MULTICAST:
				network_udp_multicast_connect();
				vTaskDelay(1000);
				break;

			case NET_MODE_UDP_BROADCAST:
				network_udp_broadcast_connect();
				vTaskDelay(1000);
				break;

			default:
				rt_kprintf("network_keep_connect unknown mode:%d\r\n", network_mode);
				vTaskDelay(1000);
				break;
		}
	}

	vTaskDelete(NULL);
}


#define PROGRAM_NAME "csvcheck"
#define AUTHORS "Robert Gamble"


/* The delimiter character */
char delimiter = CSV_COMMA;


/* The quote character */
char quote = CSV_QUOTE;

static int put_comma;

void cb1 (void *s, size_t i, void *p)
{
	char tmp[512];

	if (put_comma)
		rt_kprintf(",");

//	csv_fwrite(stdout, s, i);
	memset(tmp, 0, sizeof(tmp));
	memcpy(tmp, s, i);
	rt_kprintf("%s", tmp);
	put_comma = 1;
}

void cb2 (int c, void *p)
{
	put_comma = 0;
	rt_kprintf("\n");
}


void csv_demo(char *buf, uint16_t len)
{
	size_t pos = 0;
	//char buf[1024];
	struct csv_parser p;
	size_t bytes_read;
	size_t retval;

	if (csv_init(&p, CSV_STRICT | CSV_STRICT_FINI) != 0)
	{
		rt_kprintf("Failed to initialize csv parser\n");
		return;
	}

	bytes_read = len;

	csv_set_delim(&p, delimiter);
	csv_set_quote(&p, quote);

	if ((retval = csv_parse(&p, buf, bytes_read, cb1, cb2, NULL)) != bytes_read)
	{
		if (csv_error(&p) == CSV_EPARSE)
		{
			rt_kprintf("malformed at byte %lu\n", (unsigned long)pos + retval + 1);
			goto end;
		}
		else
		{
			rt_kprintf("Error while processing %s\n", csv_strerror(csv_error(&p)));
			goto end;
		}
	}

	if (csv_fini(&p, cb1, cb2, NULL) != 0)
		rt_kprintf("missing closing quote at end of input\n");
	else
		rt_kprintf("well-formed\n");

end:
	csv_free(&p);
	rt_kprintf("exit\n");
}

uint8_t recv_buf[1024];
static void socket_select_thread(void *arg)
{
	struct timeval tv;
	fd_set rdfds;
	fd_set wrfds;
	int maxfd = -1;
	int result = 0;

	uint8_t sock_idx = 0;
	int recv_len = 0;

	while(1)
	{
		maxfd = -1;
		FD_ZERO(&rdfds);
		FD_ZERO(&wrfds);

		for(sock_idx=0; sock_idx<MAX_CONNET_NUM; sock_idx++)
		{
			if(sock_table[sock_idx] != -1)
			{
				if(sock_table[sock_idx] > maxfd)
				{
					maxfd = sock_table[sock_idx];
				}

				FD_SET(sock_table[sock_idx], &rdfds);
				FD_SET(sock_table[sock_idx], &wrfds);
			}
		}

		if(maxfd < 0)
		{
			vTaskDelay(10);
			continue;
		}

		//int maxfdp1, fd_set *readset, fd_set *writeset, fd_set *exceptset, struct timeval *timeout
		tv.tv_sec = 0;
		tv.tv_usec = 5000;
		result = select(maxfd+1, &rdfds, NULL/*&wrfds*/, NULL, &tv);
		if(result < 0)
		{
			rt_kprintf("select < 0\r\n", recv_len, recv_buf);
		}
		else if(result == 0)
		{
			continue;
		}
		else //if(result > 0)
		{
			for(sock_idx=0; sock_idx<MAX_CONNET_NUM; sock_idx++)
			{
				if(sock_table[sock_idx] != -1 && FD_ISSET(sock_table[sock_idx], &rdfds))
				{
					memset(recv_buf, 0, sizeof(recv_buf));
					recv_len = recv(sock_table[sock_idx], recv_buf, sizeof(recv_buf), 0);
					if(recv_len < 0)
					{
						rt_kprintf("sock_idx:%d, recv < 0, close socket.\r\n", sock_table[sock_idx]);
						close(sock_table[sock_idx]);
						sock_table[sock_idx] = -1;

						continue;
					}
					else if(recv_len == 0)
					{
						rt_kprintf("sock_idx:%d, recv = 0\r\n", sock_table[sock_idx]);
					}
					else
					{
						rt_kprintf("recv[%d]:%s\r\n", recv_len, recv_buf);
						csv_demo((char *)recv_buf, recv_len);
					}
				}
			}
		}
	}

	vTaskDelete(NULL);
}


TaskHandle_t socket_connet_th;
TaskHandle_t socket_select_th;

void app_init(void)
{
	BaseType_t result = pdPASS;

	rt_kprintf("--> app_init\r\n");

	memset(sock_table, -1, sizeof(sock_table));

	result = xTaskCreate(network_keep_connect,
							"socket_con",
							512,
							NULL,
							DEFAULT_THREAD_PRIO+1,
							&socket_connet_th );
	if(result != pdPASS)
	{
		rt_kprintf("network_keep_connect create fail %d\r\n", result);
	}

	result = xTaskCreate(socket_select_thread,
							"socket_sel",
							1024,
							NULL,
							DEFAULT_THREAD_PRIO+2,
							&socket_select_th );
	if(result != pdPASS)
	{
		rt_kprintf("socket_select_thread create fail %d\r\n", result);
	}
}
