/*
 * Copyright (c) 2016, Freescale Semiconductor, Inc.
 * Copyright 2016-2019 NXP
 * All rights reserved.
 *
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

/*******************************************************************************
 * Includes
 ******************************************************************************/
#include <stdio.h>

#include "board.h"

#include "pin_mux.h"
#include "clock_config.h"
#include "fsl_gpio.h"
#include "fsl_iomuxc.h"

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

//#include "fsl_debug_console.h"
#include "netconfig.h"
#include "process.h"
#include "msh.h"

#include "cm_backtrace.h"

#if 1
#include "usb_host_config.h"
#include "usb.h"
#else
#include "usb_device_config.h"
#include "usb.h"
#include "usb_device.h"

#include "usb_device_class.h"
//#include "usb_device_cdc_acm.h"
#include "usb_device_ch9.h"

#include "usb_device_descriptor.h"
//#include "virtual_com.h"
#endif

#include "hid_generic.h"

#if (defined(FSL_FEATURE_SOC_SYSMPU_COUNT) && (FSL_FEATURE_SOC_SYSMPU_COUNT > 0U))
#include "fsl_sysmpu.h"
#endif /* FSL_FEATURE_SOC_SYSMPU_COUNT */

#if ((defined FSL_FEATURE_SOC_USBPHY_COUNT) && (FSL_FEATURE_SOC_USBPHY_COUNT > 0U))
#include "usb_phy.h"
#endif
#if defined(FSL_FEATURE_USB_KHCI_KEEP_ALIVE_ENABLED) && (FSL_FEATURE_USB_KHCI_KEEP_ALIVE_ENABLED > 0U) && \
    defined(USB_DEVICE_CONFIG_KEEP_ALIVE_MODE) && (USB_DEVICE_CONFIG_KEEP_ALIVE_MODE > 0U) &&             \
    defined(FSL_FEATURE_USB_KHCI_USB_RAM) && (FSL_FEATURE_USB_KHCI_USB_RAM > 0U)
extern uint8_t USB_EnterLowpowerMode(void);
#endif

#include "mb_httpsrv.h"
#include "mb_can.h"
#include "flash_config.h"

void BOARD_InitModuleClock(void)
{
	clock_enet_pll_config_t config = {.enableClkOutput = true, .enableClkOutput25M = false, .loopDivider = 1};
	config.enableClkOutput1 = true;
	config.loopDivider1 = 1;
	CLOCK_InitEnetPll(&config);
}

void fault_test_by_unalign(void)
{
	volatile int * SCB_CCR = (volatile int *) 0xE000ED14; // SCB->CCR
	volatile int * p;
	volatile int value;

	*SCB_CCR |= (1 << 3); /* bit3: UNALIGN_TRP. */

	p = (int *) 0x00;
	value = *p;
	rt_kprintf("addr:0x%02X value:0x%08X\r\n", (int) p, value);

	p = (int *) 0x04;
	value = *p;
	rt_kprintf("addr:0x%02X value:0x%08X\r\n", (int) p, value);

	p = (int *) 0x03;
	value = *p;
	rt_kprintf("addr:0x%02X value:0x%08X\r\n", (int) p, value);
}

void fault_test_by_div0(void)
{
	volatile int * SCB_CCR = (volatile int *) 0xE000ED14; // SCB->CCR
	int x, y, z;

	*SCB_CCR |= (1 << 4); /* bit4: DIV_0_TRP. */

	x = 10;
	y = 0;
	z = x / y;
	rt_kprintf("z:%d\n", z);
}

void dump_clock(void)
{
    rt_kprintf("OSC clock: %d\n",                  CLOCK_GetFreq(kCLOCK_OscClk));
    rt_kprintf("RTC clock: %d\n",                  CLOCK_GetFreq(kCLOCK_RtcClk));
    rt_kprintf("CPU clock: %d\n",                   CLOCK_GetFreq(kCLOCK_CpuClk));
    rt_kprintf("AHB clock: %d\n",                  CLOCK_GetFreq(kCLOCK_AhbClk));
    rt_kprintf("SEMC clock: %d\n",                 CLOCK_GetFreq(kCLOCK_SemcClk));
    rt_kprintf("IPG clock: %d\n",                  CLOCK_GetFreq(kCLOCK_IpgClk));
    rt_kprintf("ARMPLLCLK(PLL1): %d\n",            CLOCK_GetFreq(kCLOCK_ArmPllClk));
    rt_kprintf("SYSPLLCLK(PLL2/528_PLL): %d\n",    CLOCK_GetFreq(kCLOCK_SysPllClk));
    rt_kprintf("SYSPLLPDF0CLK: %d\n",              CLOCK_GetFreq(kCLOCK_SysPllPfd0Clk));
    rt_kprintf("SYSPLLPFD1CLK: %d\n",              CLOCK_GetFreq(kCLOCK_SysPllPfd1Clk));
    rt_kprintf("SYSPLLPFD2CLK: %d\n",              CLOCK_GetFreq(kCLOCK_SysPllPfd2Clk));
    rt_kprintf("SYSPLLPFD3CLK: %d\n",              CLOCK_GetFreq(kCLOCK_SysPllPfd3Clk));
    rt_kprintf("USB1PLLCLK(PLL3): %d\n",           CLOCK_GetFreq(kCLOCK_Usb1PllClk));
    rt_kprintf("USB1PLLPDF0CLK: %d\n",             CLOCK_GetFreq(kCLOCK_Usb1PllPfd0Clk));
    rt_kprintf("USB1PLLPFD1CLK: %d\n",             CLOCK_GetFreq(kCLOCK_Usb1PllPfd1Clk));
    rt_kprintf("USB1PLLPFD2CLK: %d\n",             CLOCK_GetFreq(kCLOCK_Usb1PllPfd2Clk));
    rt_kprintf("USB1PLLPFD3CLK: %d\n",             CLOCK_GetFreq(kCLOCK_Usb1PllPfd3Clk));
    rt_kprintf("Audio PLLCLK(PLL4): %d\n",         CLOCK_GetFreq(kCLOCK_AudioPllClk));
    rt_kprintf("Video PLLCLK(PLL5): %d\n",         CLOCK_GetFreq(kCLOCK_VideoPllClk));
    rt_kprintf("Enet PLLCLK ref_enetpll0: %d\n",   CLOCK_GetFreq(kCLOCK_EnetPll0Clk));
    rt_kprintf("Enet PLLCLK ref_enetpll1: %d\n",   CLOCK_GetFreq(kCLOCK_EnetPll1Clk));
    rt_kprintf("USB2PLLCLK(PLL7): %d\n",           CLOCK_GetFreq(kCLOCK_Usb2PllClk));
}

TaskHandle_t test_th;

void test_thread(void *arg)
{
	while(1)
	{
		vTaskDelay(1000);
		__disable_irq();
		rt_kprintf("--> test_thread %d\n", xTaskGetTickCount());
		__enable_irq();
	}
}
TaskHandle_t test2_th;

void test2_thread(void *arg)
{
	while(1)
	{
		vTaskDelay(1000);
		__disable_irq();
		rt_kprintf("--> test2_thread %d\n", xTaskGetTickCount());
		__enable_irq();
	}
}

void test_demo_init(void)
{
	xTaskCreate(test_thread,
								"tst",
								128,
								NULL,
								8,
								&test_th);
	xTaskCreate(test2_thread,
								"tst2",
								128,
								NULL,
								8,
								&test2_th);
}

#include "fsl_pit.h"

#define DEMO_PIT_BASEADDR PIT
#define DEMO_PIT_CHANNEL  kPIT_Chnl_0
#define PIT_LED_HANDLER   PIT_IRQHandler
#define PIT_IRQ_ID        PIT_IRQn

/* Get source clock for PIT driver */
#define PIT_SOURCE_CLOCK CLOCK_GetFreq(kCLOCK_OscClk)

volatile uint32_t FreeRTOSRunTimeTicks = 0;

/*******************************************************************************
 * Code
 ******************************************************************************/
void PIT_LED_HANDLER(void)
{
	/* Clear interrupt flag.*/
	PIT_ClearStatusFlags(DEMO_PIT_BASEADDR, DEMO_PIT_CHANNEL, kPIT_TimerFlag);

	/* Added for, and affects, all PIT handlers. For CPU clock which is much larger than the IP bus clock,
	 * CPU can run out of the interrupt handler before the interrupt flag being cleared, resulting in the
	 * CPU's entering the handler again and again. Adding DSB can prevent the issue from happening.
	 */

	FreeRTOSRunTimeTicks++;

	__DSB();
}

void pit_init(void)
{
	/* Structure of initialize PIT */
	pit_config_t pitConfig;

	/* Set PERCLK_CLK source to OSC_CLK*/
	CLOCK_SetMux(kCLOCK_PerclkMux, 1U);
	/* Set PERCLK_CLK divider to 1 */
	CLOCK_SetDiv(kCLOCK_PerclkDiv, 0U);

	/*
	 * pitConfig.enableRunInDebug = false;
	 */
	PIT_GetDefaultConfig(&pitConfig);

	/* Init pit module */
	PIT_Init(DEMO_PIT_BASEADDR, &pitConfig);

	/* Set timer period for channel 0 */                                 //100us
	PIT_SetTimerPeriod(DEMO_PIT_BASEADDR, DEMO_PIT_CHANNEL, USEC_TO_COUNT(10000U, PIT_SOURCE_CLOCK));

	/* Enable timer interrupts for channel 0 */
	PIT_EnableInterrupts(DEMO_PIT_BASEADDR, DEMO_PIT_CHANNEL, kPIT_TimerInterruptEnable);

	/* Enable at the NVIC */
	EnableIRQ(PIT_IRQ_ID);

	/* Start channel 0 */
	PIT_StartTimer(DEMO_PIT_BASEADDR, DEMO_PIT_CHANNEL);
}

uint32_t GetRunTimeCount(void)
{
	return FreeRTOSRunTimeTicks;
}

void ConfigureTimeForRunTimeStats(void)
{
	FreeRTOSRunTimeTicks = 0;

	pit_init();
}


/*!
 * @brief Main function.
 */
int ssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssmain(void)
{
	BOARD_ConfigMPU();
	BOARD_InitPins();
	BOARD_BootClockRUN();
//	BOARD_InitDebugConsole();
	debug_uart_init();
	BOARD_InitModuleClock();

	rt_kprintf("\r\n\r\n\r\n\r\n");
	rt_kprintf("--> Enter App\r\n");

	cm_backtrace_init("freertos-rt1061", "HW_V1.0.0", "SW_V1.0.0");
	dump_clock();

//	netconfig();

//	app_init();
	msh_init();
	flash_config_init();

//	usb_init();
//	mb_httpsrv_init();
//	test_demo_init();
//	mb_can_init();

//	fault_test_by_div0();
//	fault_test_by_unalign();
	
	rt_kprintf("--> vTaskStartScheduler\r\n");

	vTaskStartScheduler();

	rt_kprintf("--> Exit App\r\n");

	/* Will not get here unless a task calls vTaskEndScheduler ()*/
	return 0;
}
