#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdarg.h>

#include "pin_mux.h"
#include "clock_config.h"
#include "board.h"
#include "fsl_lpuart_edma.h"
#if defined(FSL_FEATURE_SOC_DMAMUX_COUNT) && FSL_FEATURE_SOC_DMAMUX_COUNT
#include "fsl_dmamux.h"
#endif

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"


#include "msh.h"
#include "process.h"
#include "ringbuffer.h"
#include "fifo.h"

//#define rt_kprintf(...) PRINTF(__VA_ARGS__)

Fifo_t at_fifo;
uint8_t at_buffer[128];
at_server_t at_server;



#define RT_CONSOLEBUF_SIZE		1024

static char rt_log_buf[RT_CONSOLEBUF_SIZE];

void rt_kprintf(const char *fmt, ...)
{
	va_list args;
	size_t length;

	va_start(args, fmt);
	/* the return value of vsnprintf is the number of bytes that would be
	 * written to buffer had if the size of the buffer been sufficiently
	 * large excluding the terminating null byte. If the output string
	 * would be larger than the rt_log_buf, we have to adjust the output
	 * length. */
	length = vsnprintf(rt_log_buf, sizeof(rt_log_buf) - 1, fmt, args);
	if (length > RT_CONSOLEBUF_SIZE - 1)
		length = RT_CONSOLEBUF_SIZE - 1;

	LPUART_WriteBlocking(LPUART1, (uint8_t *)rt_log_buf, length);

	va_end(args);
}

int fputc(int c, FILE *f)
{
	LPUART_WriteBlocking(LPUART1, (uint8_t *)&c, 1);
	
	return 0;
}

#define at_server_printf(...)		rt_kprintf(__VA_ARGS__);
#define at_server_printfln(...)		rt_kprintf(__VA_ARGS__);rt_kprintf("\r\n")

static at_cmd_t *at_find_cmd(const char *cmd);

/**
 * AT server request arguments parse arguments
 *
 * @param req_args request arguments
 * @param req_expr request expression
 *
 * @return  -1 : parse arguments failed
 *           0 : parse without match
 *          >0 : The number of arguments successfully parsed
 */
int at_req_parse_args(const char *req_args, const char *req_expr, ...)
{
    va_list args;
    int req_args_num = 0;

    RT_ASSERT(req_args);
    RT_ASSERT(req_expr);

    va_start(args, req_expr);

    req_args_num = vsscanf(req_args, req_expr, args);

    va_end(args);

    return req_args_num;
}

/**
 * AT server send command execute result to AT device
 *
 * @param result AT command execute result
 */
void at_server_print_result(at_result_t result)
{
    switch (result)
    {
    case AT_RESULT_OK:
        at_server_printfln("");
        at_server_printfln("OK");
        break;

    case AT_RESULT_FAILE:
        at_server_printfln("");
        at_server_printfln("ERROR");
        break;

    case AT_RESULT_NULL:
        break;

    case AT_RESULT_CMD_ERR:
        at_server_printfln("ERR CMD MATCH FAILED!");
        at_server_print_result(AT_RESULT_FAILE);
        break;

    case AT_RESULT_CHECK_FAILE:
        at_server_printfln("ERR CHECK ARGS FORMAT FAILED!");
        at_server_print_result(AT_RESULT_FAILE);
        break;

    case AT_RESULT_PARSE_FAILE:
        at_server_printfln("ERR PARSE ARGS FAILED!");
        at_server_print_result(AT_RESULT_FAILE);
        break;

    default:
        break;
    }
}

static int at_check_args(const char *args, const char *args_format)
{
    size_t left_sq_bracket_num = 0, right_sq_bracket_num = 0;
    size_t left_angle_bracket_num = 0, right_angle_bracket_num = 0;
    size_t comma_mark_num = 0;
    size_t i = 0;

    RT_ASSERT(args);
    RT_ASSERT(args_format);

    for (i = 0; i < strlen(args_format); i++)
    {
        switch (args_format[i])
        {
        case AT_CMD_L_SQ_BRACKET:
            left_sq_bracket_num++;
            break;

        case AT_CMD_R_SQ_BRACKET:
            right_sq_bracket_num++;
            break;

        case AT_CMD_L_ANGLE_BRACKET:
            left_angle_bracket_num++;
            break;

        case AT_CMD_R_ANGLE_BRACKET:
            right_angle_bracket_num++;
            break;

        default:
            break;
        }
    }

    if (left_sq_bracket_num != right_sq_bracket_num || left_angle_bracket_num != right_angle_bracket_num
            || left_sq_bracket_num > left_angle_bracket_num)
    {
        return -RT_ERROR;
    }

    for (i = 0; i < strlen(args); i++)
    {
        if (args[i] == AT_CMD_COMMA_MARK)
        {
            comma_mark_num++;
        }
    }

    if ((comma_mark_num + 1 < left_angle_bracket_num - left_sq_bracket_num)
            || comma_mark_num + 1 > left_angle_bracket_num)
    {
        return -RT_ERROR;
    }

    return RT_EOK;
}

static int at_cmd_process(at_cmd_t *cmd, const char *cmd_args)
{
    at_result_t result = AT_RESULT_OK;

    RT_ASSERT(cmd);
    RT_ASSERT(cmd_args);

    if (cmd_args[0] == AT_CMD_EQUAL_MARK && cmd_args[1] == AT_CMD_QUESTION_MARK && cmd_args[2] == AT_CMD_CR)
    {
        if (cmd->test == RT_NULL)
        {
            at_server_print_result(AT_RESULT_CMD_ERR);
            return -RT_ERROR;
        }

        result = cmd->test();
        at_server_print_result(result);
    }
    else if (cmd_args[0] == AT_CMD_QUESTION_MARK && cmd_args[1] == AT_CMD_CR)
    {
        if (cmd->query == RT_NULL)
        {
            at_server_print_result(AT_RESULT_CMD_ERR);
            return -RT_ERROR;
        }

        result = cmd->query();
        at_server_print_result(result);
    }
    else if (cmd_args[0] == AT_CMD_EQUAL_MARK
            || (cmd_args[0] >= AT_CMD_CHAR_0 && cmd_args[0] <= AT_CMD_CHAR_9 && cmd_args[1] == AT_CMD_CR))
    {
        if (cmd->setup == RT_NULL)
        {
            at_server_print_result(AT_RESULT_CMD_ERR);
            return -RT_ERROR;
        }

        if(at_check_args(cmd_args, cmd->args_expr) < 0)
        {
            at_server_print_result(AT_RESULT_CHECK_FAILE);
            return -RT_ERROR;
        }

        result = cmd->setup(cmd_args);
        at_server_print_result(result);
    }
    else if (cmd_args[0] == AT_CMD_CR)
    {
        if (cmd->exec == RT_NULL)
        {
            at_server_print_result(AT_RESULT_CMD_ERR);
            return -RT_ERROR;
        }

        result = cmd->exec();
        at_server_print_result(result);
    }
    else
    {
        return -RT_ERROR;
    }

    return RT_EOK;
}

static int at_cmd_get_name(const char *cmd_buffer, char *cmd_name)
{
    size_t cmd_name_len = 0, i = 0;

    RT_ASSERT(cmd_name);
    RT_ASSERT(cmd_buffer);

    for (i = 0; i < strlen(cmd_buffer) + 1; i++)
    {
        if (*(cmd_buffer + i) == AT_CMD_QUESTION_MARK || *(cmd_buffer + i) == AT_CMD_EQUAL_MARK
                || *(cmd_buffer + i) == AT_CMD_CR
                || (*(cmd_buffer + i) >= AT_CMD_CHAR_0 && *(cmd_buffer + i) <= AT_CMD_CHAR_9))
        {
            cmd_name_len = i;

			if(cmd_name_len >= AT_CMD_NAME_LEN)
			{
				cmd_name_len = AT_CMD_NAME_LEN - 1;
			}
            memcpy(cmd_name, cmd_buffer, cmd_name_len);
            *(cmd_name + cmd_name_len) = '\0';

            return RT_EOK;
        }
    }

    return -RT_ERROR;
}

static void server_parser(at_server_t *server)
{
#define ESC_KEY                 0x1B
#define BACKSPACE_KEY           0x08
#define DELECT_KEY              0x7F

    char cur_cmd_name[AT_CMD_NAME_LEN] = { 0 };
    at_cmd_t *cur_cmd = RT_NULL;
    char *cur_cmd_args = RT_NULL, ch, last_ch;

    RT_ASSERT(server);

    while (!IsFifoEmpty(&at_fifo) && (ESC_KEY != (ch = FifoPop(&at_fifo))))
    {
        if (server->echo_mode)
        {
            if (ch == AT_CMD_CR || (ch == AT_CMD_LF && last_ch != AT_CMD_CR))
            {
                at_server_printf("%c%c", AT_CMD_CR, AT_CMD_LF);
            }
            else if (ch == BACKSPACE_KEY || ch == DELECT_KEY)
            {
                if (server->cur_recv_len)
                {
                    server->recv_buffer[--server->cur_recv_len] = 0;
                    at_server_printf("\b \b");
                }

                continue;
            }
            else
            {
                at_server_printf("%c", ch);
            }
        }

        server->recv_buffer[server->cur_recv_len++] = ch;
        last_ch = ch;

        if(!strstr(server->recv_buffer, server->end_mark))
        {
            continue;
        }

        if (at_cmd_get_name(server->recv_buffer, cur_cmd_name) < 0)
        {
            at_server_print_result(AT_RESULT_CMD_ERR);
            goto __retry;
        }

        cur_cmd = at_find_cmd(cur_cmd_name);
        if (!cur_cmd)
        {
            at_server_print_result(AT_RESULT_CMD_ERR);
            goto __retry;
        }

        cur_cmd_args = server->recv_buffer + strlen(cur_cmd_name);
        if (at_cmd_process(cur_cmd, cur_cmd_args) < 0)
        {
            goto __retry;
        }

__retry:
        memset(server->recv_buffer, 0x00, AT_SERVER_RECV_BUFF_LEN);
        server->cur_recv_len = 0;
    }
}



extern uint32_t offset_tx_count;
extern uint32_t offset_rx_count;

static at_result_t at_exec(void)
{
	rt_kprintf("tick:%d, tx:%d, rx:%d\n", xTaskGetTickCount(), offset_tx_count, offset_rx_count);
    return AT_RESULT_OK;
}

static at_result_t ate_setup(const char *args)
{
    int echo_mode = atoi(args);

    if(echo_mode == AT_ECHO_MODE_CLOSE || echo_mode == AT_ECHO_MODE_OPEN)
    {
        at_server.echo_mode = echo_mode;
    }
    else
    {
        return AT_RESULT_FAILE;
    }

    return AT_RESULT_OK;
}

static at_result_t at_show_cmd_exec(void)
{
    extern void rt_at_server_print_all_cmd(void);

    rt_at_server_print_all_cmd();

    return AT_RESULT_OK;
}

static at_result_t at_uart_query(void)
{
//    struct rt_serial_device *serial = (struct rt_serial_device *)at_get_server()->device;
//
//    at_server_printfln("AT+UART=%d,%d,%d,%d,%d", serial->config.baud_rate, serial->config.data_bits,
//            serial->config.stop_bits, serial->config.parity, 1);

    return AT_RESULT_OK;
}

static at_result_t at_uart_setup(const char *args)
{
    int baudrate, databits, stopbits, parity, flow_control, argc;
    const char *req_expr = "=%d,%d,%d,%d,%d";

    argc = at_req_parse_args(args, req_expr, &baudrate, &databits, &stopbits, &parity, &flow_control);
    if (argc != 5)
    {
        return AT_RESULT_PARSE_FAILE;
    }

    at_server_printfln("UART baudrate : %d", baudrate);
    at_server_printfln("UART databits : %d", databits);
    at_server_printfln("UART stopbits : %d", stopbits);
    at_server_printfln("UART parity   : %d", parity);
    at_server_printfln("UART control  : %d", flow_control);

    return AT_RESULT_OK;
}


static at_result_t at_show_task_info(void)
{
	TaskStatus_t *pxTaskStatusArray;
	UBaseType_t uxArraySize, x;
	char cStatus;
	char out_str[128];
	char *state = NULL;
	uint32_t ulTotalTime, ulStatsAsPercentage;

	extern uint32_t vTaskGetStackSize( TaskHandle_t xTask );

	/* Take a snapshot of the number of tasks in case it changes while this
	function is executing. */
	uxArraySize = uxTaskGetNumberOfTasks();

	/* Allocate an array index for each task.  NOTE!  if
	configSUPPORT_DYNAMIC_ALLOCATION is set to 0 then pvPortMalloc() will
	equate to NULL. */
	pxTaskStatusArray = pvPortMalloc( uxArraySize * sizeof( TaskStatus_t ) ); /*lint !e9079 All values returned by pvPortMalloc() have at least the alignment required by the MCU's stack and this allocation allocates a struct that has the alignment requirements of a pointer. */

	if( pxTaskStatusArray != NULL )
	{
		/* Generate the (binary) data. */
		uxArraySize = uxTaskGetSystemState( pxTaskStatusArray, uxArraySize, &ulTotalTime );
		/* For percentage calculations. */
		ulTotalTime /= 100UL;

		rt_kprintf("-----------------------------------------------------------------\r\n");
		rt_kprintf("Name       No. State     bPri cPri BaseStack   Max Left    cpu\r\n");
		rt_kprintf("-----------------------------------------------------------------\r\n");

		/* Avoid divide by zero errors. */
		if( ulTotalTime > 0UL )
		{
			/* Create a human readable table from the binary data. */
			for( x = 0; x < uxArraySize; x++ )
			{
				ulStatsAsPercentage = pxTaskStatusArray[ x ].ulRunTimeCounter / ulTotalTime;

				switch( pxTaskStatusArray[x].eCurrentState )
				{
					case eRunning:
						state = "Running";
						break;

					case eReady:
						state = "Ready";
						break;

					case eBlocked:
						state = "Blocked";
						break;

					case eSuspended:
						state = "Suspended";
						break;

					case eDeleted:
						state = "Deleted";
						break;

					case eInvalid:		/* Fall through. */
					default:
						state = "Unknown";
						break;
				}

				if( ulStatsAsPercentage > 0UL )
				{
					rt_kprintf("%-10s %-3d %-9s %-3d  %-3d  0x%08x  %-6d %-6d %d%%\r\n", \
								pxTaskStatusArray[x].pcTaskName, pxTaskStatusArray[x].xTaskNumber, state, \
								pxTaskStatusArray[x].uxCurrentPriority, pxTaskStatusArray[x].uxCurrentPriority, pxTaskStatusArray[x].pxStackBase, \
								vTaskGetStackSize(pxTaskStatusArray[x].xHandle)*4, pxTaskStatusArray[x].usStackHighWaterMark*4, \
								ulStatsAsPercentage);
				}
				else
				{
					rt_kprintf("%-10s %-3d %-9s %-3d  %-3d  0x%08x  %-6d %-6d <1%%\r\n", \
								pxTaskStatusArray[x].pcTaskName, pxTaskStatusArray[x].xTaskNumber, state, \
								pxTaskStatusArray[x].uxCurrentPriority, pxTaskStatusArray[x].uxCurrentPriority, pxTaskStatusArray[x].pxStackBase, \
								vTaskGetStackSize(pxTaskStatusArray[x].xHandle)*4, pxTaskStatusArray[x].usStackHighWaterMark*4);
				}
			}
		}
		else
		{
			mtCOVERAGE_TEST_MARKER();
		}

		rt_kprintf("-----------------------------------------------------------------\r\n\r\n");

		/* Free the array again.  NOTE!  If configSUPPORT_DYNAMIC_ALLOCATION
		is 0 then vPortFree() will be #defined to nothing. */
		vPortFree( pxTaskStatusArray );
	}
	else
	{
		mtCOVERAGE_TEST_MARKER();
	}

	return AT_RESULT_OK;
}


static at_cmd_t cmd_table[] = {
	//name args_expr  test  query setup exec
	{"AT", "AT Test", NULL, NULL, NULL, at_exec},
	{"ATE", "<value>", RT_NULL, RT_NULL, ate_setup, NULL},
	{"AT&L", "AT Test", NULL, NULL, NULL, at_show_cmd_exec},
	{"AT+UART", "=<baudrate>,<databits>,<stopbits>,<parity>,<flow_control>", RT_NULL, at_uart_query, at_uart_setup, RT_NULL},
	{"ATI", "Get task info", NULL, NULL, NULL, at_show_task_info},

	{NULL, NULL, NULL, NULL, NULL, NULL}
};

static at_cmd_t *at_find_cmd(const char *cmd)
{
    size_t i = 0;

    for (i = 0; i < (sizeof(cmd_table)/sizeof(cmd_table[0])); i++)
    {
        if (!strcasecmp(cmd, cmd_table[i].name))
        {
            return &cmd_table[i];
        }
    }
    return RT_NULL;
}

/**
 *  AT server print all commands to AT device
 */
void rt_at_server_print_all_cmd(void)
{
    size_t i = 0;

    at_server_printfln("Commands list : ");

    for (i = 0; i < (sizeof(cmd_table)/sizeof(cmd_table[0])); i++)
    {
        at_server_printf("%s", cmd_table[i].name);

        if (cmd_table[i].args_expr)
        {
            at_server_printfln("%s", cmd_table[i].args_expr);
        }
        else
        {
            at_server_printf("%c%c", AT_CMD_CR, AT_CMD_LF);
        }
    }
}

/*******************************************************************************
 * Definitions
 ******************************************************************************/
#define EXAMPLE_LPUART                 LPUART1
#define EXAMPLE_LPUART_CLK_FREQ        BOARD_DebugConsoleSrcFreq()
#define LPUART_TX_DMA_CHANNEL          0U
#define LPUART_RX_DMA_CHANNEL          1U
#define LPUART_TX_DMA_REQUEST          kDmaRequestMuxLPUART1Tx
#define LPUART_RX_DMA_REQUEST          kDmaRequestMuxLPUART1Rx
#define EXAMPLE_LPUART_DMAMUX_BASEADDR DMAMUX
#define EXAMPLE_LPUART_DMA_BASEADDR    DMA0
#define EXAMPLE_LPUART_IRQHandler      LPUART1_IRQHandler
#define EXAMPLE_LPUART_IRQn            LPUART1_IRQn
/* Ring buffer size definition, please make sure to set this value large enough.
 * Otherwise, once overflow occurred, data in ring buffer will be overwritten.
 */
#define EXAMPLE_RING_BUFFER_SIZE (2048U)


/*******************************************************************************
 * Variables
 ******************************************************************************/
lpuart_edma_handle_t g_lpuartEdmaHandle;
edma_handle_t g_lpuartTxEdmaHandle;
edma_handle_t g_lpuartRxEdmaHandle;

AT_NONCACHEABLE_SECTION_INIT(uint8_t g_txBuffer[EXAMPLE_RING_BUFFER_SIZE]) = {0};
AT_NONCACHEABLE_SECTION_INIT(uint8_t g_rxBuffer[EXAMPLE_RING_BUFFER_SIZE]) = {0};
AT_NONCACHEABLE_SECTION_INIT(uint8_t g_rxringBuffer[EXAMPLE_RING_BUFFER_SIZE]) = {0};

volatile bool rxBufferEmpty = true;
volatile bool txBufferFull = false;
volatile bool txOnGoing = false;
volatile bool rxOnGoing = false;

volatile bool isIdleLineDetected = false;
volatile uint32_t ringBufferFlag = 0U;
volatile uint32_t receivedBytes = 0U;
volatile uint32_t ringBufferIndex = 0U;

#define UART_TXRX_BUFF_SIZE		2048

typedef struct {
	uint8_t data[UART_TXRX_BUFF_SIZE];
	uint32_t ridx;
	uint32_t widx;

	uint32_t size;
} ringbuffer_t;

/* Allocate TCD memory poll with ring buffer used. */
AT_NONCACHEABLE_SECTION_ALIGN(static edma_tcd_t tcdMemoryPoolPtr[1], sizeof(edma_tcd_t));

/* LPUART RX EDMA call back. */
void EXAMPLE_RxEDMACallback(edma_handle_t *handle, void *param, bool transferDone, uint32_t tcds)
{
	if (true == transferDone)
	{
//		rt_kprintf("-->EXAMPLE_RxEDMACallback:recv_len:%d, recv_idx:%d\r\n", receivedBytes, ringBufferIndex);
		ringBufferFlag++;
	}
}

/* LPUART EDMA TX user callback */
void EXAMPLE_TxEDMACallback(LPUART_Type *base, lpuart_edma_handle_t *handle, status_t status, void *userData)
{
	userData = userData;

	if (kStatus_LPUART_TxIdle == status)
	{
		txBufferFull = false;
		rt_kprintf("-->EXAMPLE_TxEDMACallback tx idle\r\n");

		txOnGoing    = false;
	}

	if (kStatus_LPUART_RxIdle == status)
	{
		rt_kprintf("-->EXAMPLE_TxEDMACallback rx idle\r\n");
		rxBufferEmpty = false;
		rxOnGoing     = false;
	}

	if (kStatus_LPUART_RxRingBufferOverrun == status)
	{
		rt_kprintf("-->EXAMPLE_TxEDMACallback rx overrun\r\n");
	}
}

void EXAMPLE_LPUART_IRQHandler(void)
{
	uint32_t status            = LPUART_GetStatusFlags(EXAMPLE_LPUART);
	uint32_t enabledInterrupts = LPUART_GetEnabledInterrupts(EXAMPLE_LPUART);

	/* If new data arrived. */
	if ((0U != ((uint32_t)kLPUART_IdleLineFlag & status)) &&
		(0U != ((uint32_t)kLPUART_IdleLineInterruptEnable & enabledInterrupts)))
	{
		(void)LPUART_ClearStatusFlags(EXAMPLE_LPUART, kLPUART_IdleLineFlag);

		isIdleLineDetected = true;

//		rt_kprintf("-->LPUART_IRQHandler idle\r\n");
	}

//	if (0U != ((uint32_t)kLPUART_TransmissionCompleteFlag & status))
//	{
//		(void)LPUART_ClearStatusFlags(EXAMPLE_LPUART, kLPUART_TransmissionCompleteFlag);
//
//		rt_kprintf("-->LPUART_IRQHandler tx done\r\n");
//	}

	if (0U != ((uint32_t)kLPUART_RxOverrunFlag & status))
	{
		(void)LPUART_ClearStatusFlags(EXAMPLE_LPUART, kLPUART_RxOverrunFlag);

		rt_kprintf("-->LPUART_IRQHandler rx overrun\r\n");
	}

	SDK_ISR_EXIT_BARRIER;
}

static void EXAMPLE_InitLPUART(void)
{
	lpuart_config_t lpuartConfig;

	/*
	 * config.baudRate_Bps = 115200U;
	 * config.parityMode = kLPUART_ParityDisabled;
	 * config.stopBitCount = kLPUART_OneStopBit;
	 * config.txFifoWatermark = 0;
	 * config.rxFifoWatermark = 0;
	 * config.enableTx = false;
	 * config.enableRx = false;
	 */
	LPUART_GetDefaultConfig(&lpuartConfig);

#if 0//def USED_MODE2
	lpuartConfig.baudRate_Bps = 115200;
	lpuartConfig.enableTx     = true;
	lpuartConfig.enableRx     = true;
#else
	lpuartConfig.baudRate_Bps = 115200;
	lpuartConfig.rxIdleType   = kLPUART_IdleTypeStopBit;
	lpuartConfig.rxIdleConfig = kLPUART_IdleCharacter2;
	lpuartConfig.enableTx     = true;
	lpuartConfig.enableRx     = true;
#endif

	LPUART_Init(EXAMPLE_LPUART, &lpuartConfig, EXAMPLE_LPUART_CLK_FREQ);
}

/* Initalize the DMA configuration for USART  TX and RX used. */
static void EXAMPLE_InitEDMA(void)
{
	edma_config_t config;

#if defined(FSL_FEATURE_SOC_DMAMUX_COUNT) && FSL_FEATURE_SOC_DMAMUX_COUNT
	/* Init DMAMUX */
	DMAMUX_Init(EXAMPLE_LPUART_DMAMUX_BASEADDR);
	/* Set channel for LPUART */
	DMAMUX_SetSource(EXAMPLE_LPUART_DMAMUX_BASEADDR, LPUART_TX_DMA_CHANNEL, LPUART_TX_DMA_REQUEST);
	DMAMUX_SetSource(EXAMPLE_LPUART_DMAMUX_BASEADDR, LPUART_RX_DMA_CHANNEL, LPUART_RX_DMA_REQUEST);
	DMAMUX_EnableChannel(EXAMPLE_LPUART_DMAMUX_BASEADDR, LPUART_TX_DMA_CHANNEL);
	DMAMUX_EnableChannel(EXAMPLE_LPUART_DMAMUX_BASEADDR, LPUART_RX_DMA_CHANNEL);
#endif

	/* Init the EDMA module */
	EDMA_GetDefaultConfig(&config);
	EDMA_Init(EXAMPLE_LPUART_DMA_BASEADDR, &config);
	EDMA_CreateHandle(&g_lpuartTxEdmaHandle, EXAMPLE_LPUART_DMA_BASEADDR, LPUART_TX_DMA_CHANNEL);
	EDMA_CreateHandle(&g_lpuartRxEdmaHandle, EXAMPLE_LPUART_DMA_BASEADDR, LPUART_RX_DMA_CHANNEL);

	/* Create LPUART DMA handle for sending data. */
	LPUART_TransferCreateHandleEDMA(EXAMPLE_LPUART, &g_lpuartEdmaHandle, EXAMPLE_TxEDMACallback, NULL,
										&g_lpuartTxEdmaHandle, &g_lpuartRxEdmaHandle);
}

/* Start ring buffer. */
static void EXAMPLE_StartRingBufferEDMA(void)
{
	edma_transfer_config_t xferConfig;

	/* Install TCD memory for using only one TCD queue. */
	EDMA_InstallTCDMemory(&g_lpuartRxEdmaHandle, (edma_tcd_t *)&tcdMemoryPoolPtr[0], 1U);

	/* Prepare transfer to receive data to ring buffer. */
	EDMA_PrepareTransfer(&xferConfig, (void *)(uint32_t *)LPUART_GetDataRegisterAddress(EXAMPLE_LPUART),
							sizeof(uint8_t), g_rxringBuffer, sizeof(uint8_t), sizeof(uint8_t), EXAMPLE_RING_BUFFER_SIZE,
							kEDMA_PeripheralToMemory);

	/* Submit transfer. */
	g_lpuartRxEdmaHandle.tcdUsed = 1U;
	g_lpuartRxEdmaHandle.tail    = 0U;
	EDMA_TcdReset(&g_lpuartRxEdmaHandle.tcdPool[0U]);
	EDMA_TcdSetTransferConfig(&g_lpuartRxEdmaHandle.tcdPool[0U], &xferConfig, tcdMemoryPoolPtr);

	/* Enable major interrupt for counting received bytes. */
	g_lpuartRxEdmaHandle.tcdPool[0U].CSR |= DMA_CSR_INTMAJOR_MASK;

	/* There is no live chain, TCD block need to be installed in TCD registers. */
	EDMA_InstallTCD(g_lpuartRxEdmaHandle.base, g_lpuartRxEdmaHandle.channel, &g_lpuartRxEdmaHandle.tcdPool[0U]);

	/* Setup call back function. */
	EDMA_SetCallback(&g_lpuartRxEdmaHandle, EXAMPLE_RxEDMACallback, NULL);

	/* Start EDMA transfer. */
	EDMA_StartTransfer(&g_lpuartRxEdmaHandle);

	/* Enable LPUART RX EDMA. */
	LPUART_EnableRxDMA(EXAMPLE_LPUART, true);

	/* Enable RX interrupt for detecting the IDLE line interrupt. */
	LPUART_EnableInterrupts(EXAMPLE_LPUART, kLPUART_IdleLineInterruptEnable | \
		kLPUART_RxOverrunInterruptEnable /*| kLPUART_TransmissionCompleteInterruptEnable*/);
	EnableIRQ(EXAMPLE_LPUART_IRQn);
}

/* Reading out the data from ring buffer. */
static void EXAMPLE_ReadRingBuffer(uint8_t *ringBuffer, uint8_t *receiveBuffer, uint32_t length)
{
	assert(ringBuffer);
	assert(receiveBuffer);
	assert(length);

	uint32_t index = length;

	/* If length if larger than ring buffer size, it means overflow occurred, need to reset the ringBufferIndex. */
	if (length > EXAMPLE_RING_BUFFER_SIZE)
	{
		ringBufferIndex = ((ringBufferIndex + length) % EXAMPLE_RING_BUFFER_SIZE);
		index = EXAMPLE_RING_BUFFER_SIZE;
	}

	while (index)
	{
		*(receiveBuffer++) = ringBuffer[ringBufferIndex++];

		if (ringBufferIndex == EXAMPLE_RING_BUFFER_SIZE)
		{
			ringBufferIndex = 0U;
			ringBufferFlag--;
		}

		index--;
	}
}

void debug_uart_init(void)
{
	/* Initialize the LPUART module. */
	EXAMPLE_InitLPUART();

	/* Intialzie the EDMA configuration. */
	EXAMPLE_InitEDMA();

	/* Start ring buffer with EDMA used. */
	EXAMPLE_StartRingBufferEDMA();
}

int EXAMPLE_WriteDataToDMA(uint8_t *data, uint32_t len)
{
	lpuart_transfer_t sendXfer;

	/* Send g_tipString out. */
	sendXfer.data     = data;
	sendXfer.dataSize = len;
	txOnGoing         = true;
	LPUART_SendEDMA(EXAMPLE_LPUART, &g_lpuartEdmaHandle, &sendXfer);

	/* Wait send finished */
	while (txOnGoing)
	{
	}
	
	return 0;
}

uint32_t EXAMPLE_GetRingBufferLengthDMA(void)
{
	uint32_t receivedBytes = EXAMPLE_RING_BUFFER_SIZE - \
			EDMA_GetRemainingMajorLoopCount(EXAMPLE_LPUART_DMA_BASEADDR, LPUART_RX_DMA_CHANNEL);

	if(receivedBytes < ringBufferIndex)
	{
		receivedBytes += EXAMPLE_RING_BUFFER_SIZE;
	}

	return receivedBytes - ringBufferIndex;
}

static void msh_parse_thread(void *arg)
#if 1
{
	uint32_t recv_len = 0;
	uint32_t index = 0;

	FifoInit( &at_fifo, at_buffer, sizeof(at_buffer) );

	memset(at_server.recv_buffer, 0x00, AT_SERVER_RECV_BUFF_LEN);
	at_server.cur_recv_len = 0;
	memcpy(at_server.end_mark, AT_CMD_END_MARK, sizeof(AT_CMD_END_MARK));

	while(1)
	{
		vTaskDelay(300);

		recv_len = EXAMPLE_GetRingBufferLengthDMA();
		if(recv_len != 0)
		{
			EXAMPLE_ReadRingBuffer(g_rxringBuffer, (uint8_t *)g_rxBuffer, recv_len);
			//rt_kprintf("-->rcev[%d]'%s'\r\n", recv_len, g_rxBuffer);
			index = 0;
			while(recv_len)
			{
				FifoPush(&at_fifo, g_rxBuffer[index++]);
				recv_len--;
			}
		}

		if(!IsFifoEmpty(&at_fifo))
		{
			server_parser(&at_server);
		}
	}
}
#else
{
	char cmd[128];
	uint8_t index = 0;
	uint32_t last_tick = 0;
	uint32_t recv_len = 0;

	memset(cmd, 0, sizeof(cmd));

	while(1)
	{
		vTaskDelay(10);

		recv_len = EXAMPLE_GetRingBufferLengthDMA();
		if(recv_len != 0)
		{
			last_tick = xTaskGetTickCount();

			EXAMPLE_ReadRingBuffer(g_rxringBuffer, (uint8_t *)g_rxBuffer, recv_len);
			if(recv_len >= (sizeof(cmd) - index))
			{
				memset(cmd, 0, sizeof(cmd));
				index = 0;
			}
			else
			{
				memcpy(cmd+index, g_rxBuffer, recv_len);
				index += recv_len;
			}
		}

		if(index == 0)
		{
			continue;
		}

		if((cmd[index-2] == '\r' && cmd[index-1] == '\n') || ((xTaskGetTickCount() - last_tick) > 60))
		{
			if(cmd[index-2] == '\r' && cmd[index-1] == '\n')
			{
				cmd[index-2] = '\0';
				index -= 2;
			}
			else if(cmd[index-1] == '\n')
			{
				cmd[index-1] = '\0';
				index -= 1;
			}

			rt_kprintf("-->rcev[%d]'%s'\r\n", strlen(cmd), cmd);

			memset(cmd, 0, sizeof(cmd));
			index = 0;
		}
	}
}
#endif

TaskHandle_t msh_th;
void msh_init(void)
{
	BaseType_t result = pdPASS;

	rt_kprintf("--> msh_init\r\n");

	result = xTaskCreate(msh_parse_thread,
							"msh",
							128,
							NULL,
							17,
							&msh_th);
	if(result != pdPASS)
	{
		rt_kprintf("msh_parse_thread create fail %d\r\n", result);
	}
}

