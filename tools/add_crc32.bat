::关闭命令的回显
@echo off

::原始bin文件路径
set firmware_path=.\rtthread-stm32f4xx.bin

::crc32校验后的bin文件路径
set crc32_firmware_path=.\rtthread-stm32f4xx-crc32.bin

.\..\tools\srecord\srec_cat.exe %firmware_path% -binary -crc32-b-e -maximum-address %firmware_path% -binary -o %crc32_firmware_path% -binary