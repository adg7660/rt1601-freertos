# quicker.em

## 一、介绍
doxygen注释的source insight的插件优化



## 二、软件架构
基于公开的queker.em修改的插件，可直接用在source insight




## 三、安装教程

1. ### 安装quicker.em宏

　　一、打开base这个工程Project->Open Project，选择base工程，即可打开；

　　二、将宏文件quicker.em添加到该base工程中；

　　三、设置宏的快捷方式：Options->Key Assignments，找到Marco:AutoExpand,添加快捷键即可，一般推介用Ctrl+Enter组合键。

2. ### HeaderFileCreate，这个宏功能用于自动创建一个.c文件的头文件。

　　使用方法：

　　　　一、创建自定义快捷菜单Options->Menu Assignments，选择Macro:HeaderFileCreate这一项，将它添加到右边Menu项的Work下，然后点Insert插入，点OK即可。

　　　　二、打开该.c文件，在work栏就能看到我们刚才添加的自定义快捷菜单HeaderFileCreate，点击它，就自动生产头文件了。

#### 使用说明

```

'/**', ' *' 可以用ctrl + enter后换行' *'

```



## 四、FAQ

### 1.关于sourceinsight版本问题

&emsp;本人在测试与开发过程中，主要基于 `Sourceinsight 4.00.0087`,在之前版本上测试时，发现部分宏无法识别；尚未在最新软件上进行测试。

### 2.关于Doexgen使用问题

&emsp;关于[Doxygen的使用方法，可以参见此博客][]。



## 五、参考链接

1. quicker.em:https://wenku.baidu.com/view/417e4b34eefdc8d376ee3259.html  

2. Doxygen:https://baike.baidu.com/item/Doxygen/1366536?fr=aladdin  

3. Source Insight 宏语言:https://www.sourceinsight.com/doc/v4/userguide/index.html#t=Manual%2FMacro_Language%2FMacro_Language.htm  

4. Doxygen注释风格:https://my.oschina.net/zhfish/blog/35422  

5. Doxygen的使用方法，可以参见此博客:https://blog.csdn.net/chenyujing1234/article/details/19115319
